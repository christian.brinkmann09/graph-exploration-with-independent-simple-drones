
from visualization.tvisualizer import TVisualizer, latex_colors, get_stats_id

import sys

class TourLengthPerAlgorithmPlot(TVisualizer):

    def visualize(self, stats, algorithms, iterations, vertex_counts, edge_scaling_factors):
        result = ""
        for edge_scaling_factor in sorted(edge_scaling_factors):
            color_index = 0
            result += """
            
            
            \\begin{figure}[htbp]
                \\centering
                \\begin{tikzpicture}[scale=1]
                    \\begin{axis}[
                        xlabel={$n$: vertices in graph},
                        ymode=log,
                        ylabel={$\\frac{|t|}{n}$: tour length / vertices},
                        axis line style={->},
                        width=0.8\\textwidth,
                        height=0.5\\textwidth,
                        grid=both,
                        grid style={line width=.1pt, draw=gray!10},
                        major grid style={line width=.2pt,draw=gray!50},
                        legend pos=outer north east,
                        ]"""
            vertex_counts = list(sorted(vertex_counts))
            for algorithm in algorithms:
                for iteration in range(iterations):
                    header = """
                    \\addplot[""" + latex_colors[color_index % len(latex_colors)] + ", thick] coordinates {"""
                    color_index += 1
                    content = ""
                    for vc in vertex_counts:
                        stats_id = get_stats_id(algorithm, iteration, vc, edge_scaling_factor)
                        if stats_id in stats:
                            stat = stats[stats_id]
                            title = stat["name"].replace("GreedyVertexCount","GVC") + " it. "+str(iteration)+"\n"

                            content += "(" + str(stat["vertices"]) + "," + str(stat["avg_len"] / stat["vertices"]) + ")"
                        else:
                            title = None
                    if title is not None:
                        footer = (
                            """};
                            \\addlegendentry{"""+ title+ """
                        }""")
                        result += header + content + footer + "\n"
            result += """
                    \\end{axis}
                \\end{tikzpicture}
                \\caption{"""+(", ".join(algorithms))+""" running on """+str(stat["samples"])+""" samples on each """+stat["graph"]+""", edge probability $p=\\frac{"""+str(edge_scaling_factor)+"""}{n}$ \\protect\\footnotemark}

            \\label{fig:}
        \\end{figure}
        
        \\footnotetext{
            Visualized using \\texttt{"""+(" ".join(sys.argv)).replace("_","\\_")+"""}. Data generated using \\texttt{"""+str(self.args).replace("_","\\_")+"""}
        }
        """
        return result