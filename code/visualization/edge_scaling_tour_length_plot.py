
from visualization.tvisualizer import TVisualizer, latex_colors, get_stats_id

import sys

class EdgeScalingTourLengthPlot(TVisualizer):

    def visualize(self, stats, algorithms, iterations, vertex_counts, edge_scaling_factors):
        result = """
        \\begin{figure}[htbp]
            \\centering
            \\begin{tikzpicture}[scale=1]
                \\begin{axis}[
                    xlabel={$n$: vertices in graph},
                    ylabel={$\\frac{|t|}{n}$: tour length / vertices},
                    axis line style={->},
                    width=0.8\\textwidth,
                    height=0.5\\textwidth,
                    grid=both,
                    grid style={line width=.1pt, draw=gray!10},
                    major grid style={line width=.2pt,draw=gray!50},
                    legend pos=outer north east,
                    ]"""
        for edge_scaling_factor in edge_scaling_factors:
            header = """
                    \\addplot[""" + latex_colors[edge_scaling_factors.index(edge_scaling_factor)] + ", thick] coordinates {"""
            content = ""
            for algorithm in algorithms:
                for iteration in range(iterations):
                    for vc in vertex_counts:
                        stats_id = get_stats_id(algorithm, iteration, vc, edge_scaling_factor)
                        if stats_id in stats:
                            stat = stats[stats_id]
                            title = stat["name"] + "($\\alpha$=" + "{:.2f}".format(edge_scaling_factor) + ", d="+"{:.2f}".format(stat["avg_degree"])+")\n"

                            content += "(" + str(stat["vertices"]) + "," + str(stat["avg_len"] / stat["vertices"]) + ")"
                        else:
                            title = None
                    if title is not None:
                        footer = (
                            """};
                            \\addlegendentry{"""+ title+ """
                        }""")
                        result += header + content + footer + "\n"
        result += """
                    \\end{axis}
                \\end{tikzpicture}
                \\caption{"""+(", ".join([type(algorithm).__name__ for algorithm in algorithms]))+""" running on """+str(stat["samples"])+""" samples on each """+stat["graph"]+""", edge probability $p=\\frac{\\alpha}{n}$ \\protect\\footnotemark}

            \\label{fig:}
        \\end{figure}
        
        \\footnotetext{
            Visualized using \\texttt{"""+(" ".join(sys.argv)).replace("_","\\_")+"""}. Data generated using \\texttt{"""+str(self.args).replace("_","\\_")+"""}
        }
        """
        return result