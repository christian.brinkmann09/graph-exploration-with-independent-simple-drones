import networkx as nx
import time

from algorithm.talgorithm import TAlgorithm

class OfflineSolver(TAlgorithm):
    
    debug_print = False

    def run(self, G, origin, ids=None, count=1):
        walks = [[origin]]
        total_walk_amount = 0
        
        if len(G.nodes) <= 1:
            return walks[0]
        
        ts = time.time()
        start_time = time.time()
        while True:
            total_walk_amount += len(walks)
            if time.time() - ts > 1:
                print("Calculated "+str(len(walks))+" walks of length "+str(len(walks[0]))+" in "+"{:.2f}".format(time.time()-ts)+"s")
                ts = time.time()
            new_walks = list()
            for walk in walks:
                for v in G.neighbors(walk[-1]):
                    w = walk + [v]
                    if v == origin and len(set(w)) == len(G.nodes):
                        total_walk_amount += len(new_walks) + 1
                        
                        if time.time() - start_time > 1:
                            print("-----------------------")
                            print("In total: "+str(total_walk_amount)+ " walks calculated in "+"{:.2f}".format(time.time()-start_time)+"s")
                        return w
                    new_walks.append(w)
            walks = new_walks
            
    def is_multi_iteration_supported(self):
        return False
    
    def work_required(self, n, p):
        return n ** int(max(1,p))