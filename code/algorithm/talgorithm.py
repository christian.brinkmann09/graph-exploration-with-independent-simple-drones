
class TAlgorithm:
    def __init__(self):
        pass

    def run(self, G, origin, ids=None, count=1):
        raise NotImplementedError("Run method not implemented in TAlgorithm superclass.")
    
    def _neighbors(self, G, v, ids=None):
        if ids:
            return sorted(G.neighbors(v), key=lambda x: ids[x])
        else:
            return sorted(G.neighbors(v))
        
    def is_multi_iteration_supported(self):
        return True
    
    def is_multi_searcher_supported(self):
        return False
    
    def work_required(self, n, p):
        return n * n // p
    
class NoAlg(TAlgorithm):
    def run(self, G, start, ids=None, count=1):
        return list()
