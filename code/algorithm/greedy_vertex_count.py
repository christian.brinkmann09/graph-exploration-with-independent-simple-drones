import networkx as nx

from algorithm.talgorithm import TAlgorithm

class GreedyVertexCount(TAlgorithm):

    def run(self, G, origin, ids=None, count=1):
        current = origin
        unvisited_vertex_count = len(G.nodes)
        traversal = [origin]
        # otherwise this throws an error on trivial graphs
        if len(G.nodes) <= 1:
            return traversal
        # figure out which iteration we are at
        if "iteration" in G.graph:
            iteration = G.graph["iteration"]
        else:
            iteration = 0
        # prepare keys
        visited_key = "visited_" + str(iteration)
        explored_key = "explored_"+str(iteration)
        # set all visited counts for the current iteration to 0, except for the origin
        for node in G.nodes:
            G.nodes[node][visited_key] = 0
        G.nodes[origin][visited_key] = 1
        # the tour is done if the drone has reached the origin has visited all vertices
        while current != origin or unvisited_vertex_count > 0:
            next_node = None
            
            # use secondary pebble color (explored_key) to mark unexplored vertices as explored
            if not explored_key in G.nodes[current]:
                G.nodes[current][explored_key] = 1
                unvisited_vertex_count -= 1

            # if we have seen all vertices, go for the origin even if it's not the least visited neighbor
            if unvisited_vertex_count == 0 and origin in G.neighbors(current):
                next_node = origin
            else:
                # find list of least visited neighbors
                possible_next_nodes = list()
                for node in self._neighbors(G, current, ids=ids):
                    if len(possible_next_nodes) == 0:
                        possible_next_nodes.append(node)
                    else:
                        if G.nodes[possible_next_nodes[0]][visited_key] > G.nodes[node][visited_key]:
                            possible_next_nodes = list()
                            possible_next_nodes.append(node)
                        elif G.nodes[possible_next_nodes[0]][visited_key] == G.nodes[node][visited_key]:
                            possible_next_nodes.append(node)
                # if we are not in the first iteration we can use the pebbles left behind by prior iterations to reduce the list of possible_next_nodes further
                if iteration > 0:
                    proposed_next_nodes = possible_next_nodes
                    tiebreaker_iteration = 0
                    while len(possible_next_nodes) > 1:
                        tiebreaker_key = "visited_"+str(tiebreaker_iteration)
                        nodes_with_value = list(filter(lambda y: tiebreaker_key in G.nodes[y], possible_next_nodes))
                        proposed_next_nodes = list()
                        # 
                        if len(nodes_with_value) > 0:
                            lowest_visited_count = min([G.nodes[x][tiebreaker_key] for x in nodes_with_value])
                            proposed_next_nodes = list(filter(lambda y: G.nodes[y][tiebreaker_key] == lowest_visited_count, possible_next_nodes))
                        if len(proposed_next_nodes) == 0:
                            proposed_next_nodes = possible_next_nodes
                            break
                        elif len(proposed_next_nodes) == 1:
                            break
                        else:
                            possible_next_nodes = proposed_next_nodes
                            tiebreaker_iteration += 1
                    next_node = proposed_next_nodes[0]
                else:
                    next_node = possible_next_nodes[0]

            traversal.append(next_node)
            G.nodes[next_node][visited_key] += 1
            current = next_node
            G.graph["iteration"] = iteration + 1
        return traversal