import networkx as nx

from graph.tgraph import TUnGraph

from algorithm.talgorithm import TAlgorithm

class DFS_ID(TAlgorithm):

    def run(self, G, origin, ids=None, count=1):
        current = origin
        traversal = [origin]
        iteration = 0
        # set pebbles of all vertices to 0
        visited_key = "visited_" + str(iteration)
        for node in G.nodes:
            G.nodes[node][visited_key] = 0
        # repeat until we reach the origin and all neighbors have been visited
        while current != origin or any([G.nodes[n][visited_key]==0 for n in self._neighbors(G, current)]):
            
            # set "id" which consists of a move counter and a depth counter
            # first find the most pebbles any neighbor of current uses
            max_neighbor_pebbles = 0
            for node in self._neighbors(G, current, ids=ids):
                if node != current and G.nodes[node][visited_key] > max_neighbor_pebbles:
                    max_neighbor_pebbles = G.nodes[node][visited_key]
            # then adjust the pebbles of the current pebbles accordingly
            if G.nodes[current][visited_key] == 0:
                if max_neighbor_pebbles == 0:
                    # set the pebble count of the origin to 1
                    G.nodes[current][visited_key] = 1
                else:
                    # if a vertex is visited for the first time, increase both the move counter and depth counter by 1
                    G.nodes[current][visited_key] = max_neighbor_pebbles + len(G.nodes) + 1 + 1
            else:
                # If the pebble was visited prior, we only want to increase the move counter while keeping the depth counter as is
                G.nodes[current][visited_key] = ((max_neighbor_pebbles // (len(G.nodes)+1)) + 1 ) * (len(G.nodes) + 1) + (G.nodes[current][visited_key] % (len(G.nodes)+1))
                        
            
            next_node = None
            possible_next_nodes = list()
            # find unvisited neighbor if it exists
            for node in self._neighbors(G, current, ids=ids):
                if G.nodes[node][visited_key] == 0:
                    possible_next_nodes.append(node)
                    
            # go back to the previous node if there are no unvisited children
            if len(possible_next_nodes) == 0:
                for node in self._neighbors(G, current, ids=ids):
                    # Figure out the previous vertex based on the depth counter of the current and its neighboring vertex, the prior vertex has a depth counter that is 1 lower, and there will be exactly one vertex fitting this (except for the origin)
                    if G.nodes[node][visited_key] % (len(G.nodes)+1) == (G.nodes[current][visited_key] % (len(G.nodes)+1)) - 1:
                        possible_next_nodes.append(node)
            # chose next vertices, in case of multiple possible next vertices, use the one with lowest ID
            next_node = possible_next_nodes[0]
            traversal.append(next_node)
            current = next_node
        return traversal
    
    
    def is_multi_iteration_supported(self):
        return False