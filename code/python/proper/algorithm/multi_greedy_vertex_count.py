import networkx as nx

from algorithm.talgorithm import TAlgorithm

class MultiGreedyVertexCount(TAlgorithm):
    
    # shared static information (ROM)
    visited_key = None
    explored_key = None
    start = None
    G = None
    ids = None
    
    # values for simulating drones
    current = list()
    unvisited_count = list()
    # only used for analysis of the behaviour
    walk = list()
    
    # values for the base station
    count = 0
    iteration = 0
    combined_vistied_count = 1
    
    def setup(self, count, G, origin, ids):
        self.start = origin
        self.current = [0 for _ in range(count)]
        self.unvisited_count = [len(G.nodes)-1 for _ in range(count)]
        self.walk = [list([origin]) for _ in range(count)]
        self.count = count
        self.G = G
        self.ids = ids
        if "iteration" in G.graph:
            self.iteration = G.graph["iteration"]
        else:
            self.iteration = 0
        self.visited_key = "visited_" + str(self.iteration)
        self.explored_key = "explored_"+str(self.iteration)
        for node in G.nodes:
            G.nodes[node][self.visited_key] = 0
        G.nodes[origin][self.explored_key] = 1
        self.combined_vistied_count = 1
        
    def execute(self, index, cheat=False):
        current = self.current[index]
        next_node = None
        
        if not cheat:
            if current == self.start:
                # interact with the base station
                print(len(self.G.nodes))
                print(self.unvisited_count[index])
                self.combined_vistied_count += len(self.G.nodes) - 1 - self.unvisited_count[index]
                self.unvisited_count[index] = len(self.G.nodes) - 1
                if self.combined_vistied_count >= len(self.G.nodes):
                    return True # This searcher has finished the task
        else:
            if self.combined_vistied_count >= len(self.G.nodes):
                return True # Finished in cheat mode
            
        if not self.explored_key in self.G.nodes[current]:
            self.G.nodes[current][self.explored_key] = 1
            if cheat:
                self.combined_vistied_count += 1
            else:
                self.unvisited_count[index] -= 1

        if self.unvisited_count[index] == 0 and self.start in self.G.neighbors(current):
            # if we have seen all nodes, go for the origin even if it's not the least visited neighbor
            next_node = self.start
        else:
            possible_next_nodes = list()
            for node in self._neighbors(self.G, current, ids=self.ids):
                if len(possible_next_nodes) == 0:
                    possible_next_nodes.append(node)
                else:
                    if self.G.nodes[possible_next_nodes[0]][self.visited_key] > self.G.nodes[node][self.visited_key]:
                        possible_next_nodes = list()
                        possible_next_nodes.append(node)
                    elif self.G.nodes[possible_next_nodes[0]][self.visited_key] == self.G.nodes[node][self.visited_key]:
                        possible_next_nodes.append(node)
            if self.iteration > 0:
                proposed_next_nodes = possible_next_nodes
                tiebreaker_iteration = 0
                while len(possible_next_nodes) > 1:
                    tiebreaker_key = "visited_"+str(tiebreaker_iteration)
                    nodes_with_value = list(filter(lambda y: tiebreaker_key in self.G.nodes[y], possible_next_nodes))
                    proposed_next_nodes = list()
                    if len(nodes_with_value) > 0:
                        lowest_visited_count = min([self.G.nodes[x][tiebreaker_key] for x in nodes_with_value])
                        proposed_next_nodes = list(filter(lambda y: self.G.nodes[y][tiebreaker_key] == lowest_visited_count, possible_next_nodes))
                    if len(proposed_next_nodes) == 0:
                        proposed_next_nodes = possible_next_nodes
                        break
                    elif len(proposed_next_nodes) == 1:
                        break
                    else:
                        possible_next_nodes = proposed_next_nodes
                        tiebreaker_iteration += 1
                next_node = proposed_next_nodes[0]
            else:
                next_node = possible_next_nodes[0]

        self.walk[index].append(next_node)
        self.G.nodes[next_node][self.visited_key] += 1
        self.current[index] = next_node
        self.G.graph["iteration"] = self.iteration + 1
        return False # Signals that this searcher is not done yet

    def run(self, G, start, ids=None, count=1):
        self.setup(count=count, G=G, origin=start, ids=ids)
        while True:
            #print("-------------")
            for i in range(self.count):
                if self.execute(i, cheat=True):
            #        print("DONE")
                    return self.walk
            #print(self.walk)
            #print(self.combined_vistied_count)
            #input()
            
    def is_multi_searcher_supported(self):
        return True