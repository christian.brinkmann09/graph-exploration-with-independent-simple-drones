import networkx as nx

from algorithm.talgorithm import TAlgorithm
class DFS(TAlgorithm):

    def run(self, G, origin, ids=None, count=1):
        current = origin
        traversal = [origin]
        # otherwise this throws an error on trivial graphs
        if len(G.nodes) <= 1:
            return traversal
        iteration = 0
        # visited_<iteration> keeps track of the number of visits to each vertex at each iteration
        # pebbles[vertex] corresponds to G.nodes[vertex][visited_0]
        # as DFS does not support multiple iterations
        visited_key = "visited_" + str(iteration)
        # Initially, all vertices are marked as unvisited except for the origin
        for node in G.nodes:
            G.nodes[node][visited_key] = 0
        G.nodes[origin][visited_key] = 1
        # Continue until we return to the origin and all neighboring vertices are visited
        while current != origin or any(G.nodes[n][visited_key] == 0 for n in self._neighbors(G, current, ids=ids)):
            
            # Set local "id" (or traversal depth) that tells us from which vertex we entered
            # This works on an unlabeled graph but is not deterministic
            if G.nodes[current][visited_key] == 0:
                max_id = 0
                for node in self._neighbors(G, current, ids=ids):
                    if node != current and G.nodes[node][visited_key] > max_id:
                        max_id = G.nodes[node][visited_key]
                G.nodes[current][visited_key] = max_id + 1
                        
            # Determine the next node to visit
            next_node = None
            # List of possible nodes to visit next
            possible_next_nodes = []
            # Find an unvisited neighbor if it exists
            for node in self._neighbors(G, current, ids=ids):
                if G.nodes[node][visited_key] == 0:
                    possible_next_nodes.append(node)
                    
            # If there are no unvisited neighbors, go back to the previous node
            if not possible_next_nodes:
                for node in self._neighbors(G, current, ids=ids):
                    if G.nodes[node][visited_key] == G.nodes[current][visited_key] - 1:
                        possible_next_nodes.append(node)
            # Advance to the next node
            next_node = possible_next_nodes[0]
            traversal.append(next_node)
            current = next_node
        # Return the traversal path for evaluation
        # Note: This is just for evaluation and not part of proper drone exploration, 
        # as it contradicts the no-internal-memory property
        return traversal

    
    
    def is_multi_iteration_supported(self):
        return False