import argparse
import argcomplete
import inspect
import os
import sys
import json
import time
import copy
import math
import re
from datetime import timedelta

from networkx.drawing.nx_pydot import graphviz_layout

from graph.tgraph import *
from graph import ladder_graph
from graph import randgraph
from graph import completegraph
from graph import cyclegraph
from graph import treegraph

from algorithm.talgorithm import *
from algorithm import greedy_vertex_count
from algorithm import dfs
from algorithm import dfs_id
from algorithm import offline_solver
from algorithm import multi_greedy_vertex_count

from visualization.tvisualizer import *
from visualization import tvisualizer
from visualization import edge_scaling_tour_length_plot
from visualization import compared_to_offline_plot
from visualization import tour_length_per_algorithm_plot
from visualization import pebble_count_per_algorithm_plot

debug = False

# allows us to find matching classes by their names
def get_subclasses(superclass):
    subclasses = []

    modules_copy = dict(sys.modules)
    for module_name, module in modules_copy.items():
        if not module:
            continue
        try:
            for name, obj in inspect.getmembers(module):
                if (
                    inspect.isclass(obj)
                    and issubclass(obj, superclass)
                    and obj != superclass
                ):
                    subclasses.append(obj)
        except ModuleNotFoundError as e:
            if debug:
                print(f"ModuleNotFoundError in module {module_name}: {e}")
        except ImportError as e:
            if debug:
                print(f"ImportError in module {module_name}: {e}")
        except Exception as e:
            if debug:
                print(f"Unexpected error in module {module_name}: {e}")
    return subclasses


# Function to list graph files for autocompletion
def complete_graph(prefix, parsed_args, **kwargs):
    return get_subclasses(TDiGraph) + get_subclasses(TUnGraph)

def calculate_elapsed_time(time_left):
    formatted_time = timedelta(seconds=time_left)
    # Splitting the components
#    days, hours, minutes, _ = formatted_time.split(":")
    days = formatted_time.days
    hours, remainder = divmod(formatted_time.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    # Create a list to store the non-zero components
    time_components = []

    # Add non-zero components to the list
    if days != 0:
        time_components.append(f"{days} days")
    if hours != 0:
        time_components.append(f"{hours} hours")
    if minutes != 0:
        time_components.append(f"{minutes} minutes")
    if seconds != 0:
        time_components.append(f"{seconds} seconds")

    # Join the components into a human-readable string
    elapsed_time_str = ", ".join(time_components)

    return elapsed_time_str

def progress_bar(current, total, text, bar_length=20, total_start_time=None):
    fraction = current / total

    arrow = int(fraction * bar_length - 1) * '-' + '>'
    padding = int(bar_length - len(arrow)) * ' '

    ending = '\n' if current == total else '\r'
    
    time_left = None
    if total_start_time is not None:
        if fraction <= 0 or fraction >= 1:
            time_left = None
        else:
            time_left = (time.time() - total_start_time) * (1-fraction) / fraction

    print(f'{text}: [{arrow}{padding}] {int(fraction*100)}% '+('ETA: '+calculate_elapsed_time(time_left)+'' if time_left is not None else '')+'            ', end=ending)

def log_progress(current, total, total_start_time):
    progress_bar(current, total, "Simulating graph exploration", total_start_time=total_start_time)

def get_stats_id(algorithm, iteration, n, p, searcher):
    return tvisualizer.get_stats_id(algorithm, iteration, n, p, searcher)


def _register_stats(stats, iteration, G, traversal, timedelta, edge_scaling_factor=1, seacher_count=1):
    stats["samples"] += 1
    stats["length"] += (len(traversal) - 1) if len(traversal) > 1 else 0
    stats["avg_len"] = stats["length"] / stats["samples"]
    stats["avg_degree"] = (
        stats["avg_degree"] * (stats["samples"] - 1)
        + sum([G.degree(n) for n in G.nodes]) / len(G.nodes) 
    ) / stats["samples"]
    stats["min_degree"] = min([G.degree(n) for n in G.nodes])
    visited_key = "visited_" + str(iteration)
    pebbles_used = 0
    stats["edge_scaling_factor"] = edge_scaling_factor
    stats["searcher"] = seacher_count
    for node in G.nodes:
        if visited_key in G.nodes[node]:
             pebbles_used += G.nodes[node][visited_key]
    stats["pebbles"] += pebbles_used
    stats["avg_pebbles"] = stats["pebbles"] / len(G.nodes) / stats["samples"]
    stats["vertices"] = len(G.nodes)
    stats["tour_lengths"].append((len(traversal) - 1) if len(traversal) > 1 else 0)
    stats["tour_pebbles"].append(pebbles_used)
    stats["iteration"] = iteration
    stats["time"] += timedelta


def comma_separated_int_list(string):
    try:
        return [int(x) for x in string.split(",")]
    except ValueError:
        raise argparse.ArgumentTypeError(
            "Invalid list of integers: '{}'".format(string)
        )


def comma_separated_float_list(string):
    try:
        return [float(x) for x in string.split(",")]
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid list of floats: '{}'".format(string))

def comma_separated_str_list(string):
    try:
        return [str(x) for x in string.split(",")]
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid list of strings: '{}'".format(string))
    
def parse_vertices(value):
    # Check if the input is in the range format 'N-M'
    range_match = re.match(r'^(\d+)-(\d+)$', value)
    if range_match:
        start, end = int(range_match.group(1)), int(range_match.group(2))
        if start > end:
            raise argparse.ArgumentTypeError(f"Invalid range: start {start} is greater than end {end}.")
        return list(range(start, end + 1))
    
    # Check if the input is in the comma-separated format '1,2,3,4,5'
    list_match = re.match(r'^(\d+(,\d+)*)$', value)
    if list_match:
        return list(map(int, value.split(',')))

    raise argparse.ArgumentTypeError(f"Invalid input format: {value}. Expected 'N-M' or '1,2,3,...'.")

layouts = {
    "bipartite": nx.bipartite_layout,
    "circular": nx.circular_layout,
    "kamada_kawai": nx.kamada_kawai_layout,
    "planar": nx.planar_layout,
    "random": nx.random_layout,
    "rescale": nx.rescale_layout,
    "rescale_dict": nx.rescale_layout_dict,
    "shell": nx.shell_layout,
    "spring": nx.spring_layout,
    "spectral": nx.spectral_layout,
    "spiral": nx.spiral_layout,
    "multipartite": nx.multipartite_layout,
    "graphviz": graphviz_layout
}

# Main function
def main():
    parser = argparse.ArgumentParser(description="Run algorithms on a graph.")
    
    
    subparsers = parser.add_subparsers(dest='subcommand', required=True, help="Subcommand to execute.")

    # RUN command

    parser_run = subparsers.add_parser('run', help='Run algorithms on a graph')
    
    parser_run.add_argument(
        "--silent", action="store_true", help="Don't print anything that is not explicitly enabled"
    )
    
    parser_run.add_argument("algorithms", nargs="+", help="Algorithms to run.")
    
    parser_run.add_argument("graph", type=str, help="Graph (class) to operate on.").completer = (
        complete_graph
    )
    
    parser_run.add_argument(
        "--iterations", type=int, default=1, help="Number of iterations"
    )
    
    parser_run.add_argument("--seed", type=int, default=0, help="Seed for randomness, is increased by 1 per sample")
    
    parser_run.add_argument(
        "--vertices",
        type=parse_vertices,
        default=[10],
        help="vertex amounts, either list or range (e.g., 1,2,3,5,10 or 1-5)",
    )
    
    parser_run.add_argument(
        "--min-degree",
        type=comma_separated_int_list,
        default=[1],
        help="List of minimum degree for random graphs (e.g., 1,2,3,5,10)",
    )
    
    parser_run.add_argument('--dump-raw-file', type=str, nargs='?', default=None, help='Specify a file where the --dump-raw output gets redirected to.')
    
    parser_run.add_argument('--show-latex-file', type=argparse.FileType('w'), nargs='?', default=None, help='Specify a file where the --show-latex output gets redirected to.')
    
    parser_run.add_argument('--show-file', type=str, nargs='?', default=None, help='Specify a file where the --show-file output gets redirected to.')
    
    parser_run.add_argument('--scale', nargs="?", type=float, default=1, help="Scale the visual representation.")
    
    parser_run.add_argument(
        "--edge-scaling-factor",
        type=comma_separated_float_list,
        default=[5],
        help="List of edge scaling factors (alpha) separated by commas (e.g., 3,5,10). Edge probability is 1/n*alpha",
    )
    parser_run.add_argument(
        "--searcher",
        type=comma_separated_int_list,
        default=[1],
        help="List of searchers to be used, separated by commas (e.g., 3,5,10), only relevant for multi-searcher algorithms",
    )
    parser_run.add_argument(
        "--shuffle-ids", action="store_true", help="Reshuffle the IDs of vertices."
    )
    
    parser_run.add_argument(
        "--show", action="store_true", help="Show the graph visualization"
    )
    parser_run.add_argument(
        "--show-latex", action="store_true", help="Print latex code for the graph visualization"
    )
    parser_run.add_argument(
        "--progress", action="store_true", help="Print progress bar"
    )
    parser_run.add_argument(
        "--cheat", action="store_true", help="Allow a multi-seacher exploration to terminate without returning to the origin to deliver information."
    )
    parser_run.add_argument(
        "--dump-raw", action="store_true", help="Print raw stats to log"
    )
    
    parser_run.add_argument(
        "--samples", type=int, default=1, help="Number of samples (default: 1)"
    )
    
    parser_run.add_argument(
        "--layout",
        type=str,
        default="circular",
        choices=list(layouts.keys()),
        help="Layouts for show/draw. ",
    )
    
    parser_run.add_argument(
        "--graphviz-prog",
        type=str,
        default=None,
        choices=['dot', 'neato', 'circo', 'twopi', 'fdp', 'sfdp', 'patchwork'],
        help="Graphviz layouts for --show/--latex, requires --layout graphviz. See https://graphviz.org/docs/layouts/",
    )
    
    parser_run.add_argument("--visualizer", type=comma_separated_str_list, default=[], help="Visualization function to employ.")
    
    parser_run.add_argument('--visualizer-files', type=comma_separated_str_list, nargs='?', default=[], help='Specify files where the --visualizer output gets redirected to')
    
    # SHOW subcommand
    
    parser_show = subparsers.add_parser('show', help='Display stats from .json')
    
    parser_show.add_argument(
        "--silent", action="store_true", help="Don't print anything that is not explicitly enabled"
    )
    
    parser_show.add_argument(
        "input", type=str, default=None, help="Load stat .json file(s) for visualization purposes."
    )
    
    
    parser_show.add_argument("--visualizer", type=comma_separated_str_list, default=[], help="Visualization function to employ.")
    
    parser_show.add_argument('--visualizer-files', type=comma_separated_str_list, nargs='?', default=[], help='Specify files where the --visualizer output gets redirected to')
    
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    # Get all subclasses of TAlgorithm
    algorithms = get_subclasses(TAlgorithm)
    
    if args.subcommand == "run":
        # Create instances of selected algorithms
        selected_algorithms = []
        for algorithm_name in args.algorithms:
            exists = False
            for algorithm in algorithms:
                if algorithm.__name__ == algorithm_name:
                    exists = True
                    selected_algorithms.append(algorithm())
            
            if not exists:
                print(
                    "Invalid algorithm '"+algorithm_name+"'selected. Possible algorithms("
                    + str(len(algorithms))
                    + "): ",
                    end="",
                )
                print(", ".join([algorithm.__name__ for algorithm in algorithms]))
                exit()

        # select the graph
        graphs = get_subclasses(TDiGraph) + get_subclasses(TUnGraph)
        selected_graph = None
        for graph in graphs:
            if graph.__name__ == args.graph:
                selected_graph = graph
                
        # select the visualizers
        visualizers = get_subclasses(TVisualizer)
        selected_visualizers = []
        for visualizer_name in args.visualizer:
            for visualizer in visualizers:
                if visualizer.__name__ == visualizer_name:
                    selected_visualizers.append(visualizer)
        if len(args.visualizer) > 0 and len(selected_visualizers) == 0:
            print("Could not find matching visualizer for '"+",".join(args.visualizer)+"'")
            print("Known Visualizers: "+",".join([visualizer.__name__ for visualizer in visualizers]))
            exit()

        if len(args.visualizer_files) != 0 and len(selected_visualizers) != len(args.visualizer_files):
            print("When specifying --visualizer-files as outputs, you need to match the number of outputs to the number of visualizers.")
            exit()

        if len(selected_algorithms) == 0:
            print(
                "No algorithm selected. Possible algorithms("
                + str(len(algorithms))
                + "): ",
                end="",
            )
            print(", ".join([algorithm.__name__ for algorithm in algorithms]))
            exit()

        if selected_graph is None:
            print("No graph selected. Possible graphs(" + str(len(graphs)) + "): ", end="")
            print(", ".join([graph.__name__ for graph in graphs]))
            exit()
            
        if args.layout not in layouts:
            print("Invalid layout '"+str(args.layout)+"'.")
            print("Choices: "+", ".join(list(layouts.keys())))
            exit()
        layout = layouts[args.layout]

        # initialize the stats

        total_start_time = time.time()
        stats = dict()
        stats["info"] = dict()
        stats["info"]["args"] = " ".join(sys.argv)
        stats["info"]["time"] = 0.0
        for algorithm in selected_algorithms:
            for i in range(
                args.iterations if algorithm.is_multi_iteration_supported() else 1
            ):
                for count in (args.searcher if algorithm.is_multi_searcher_supported() else [1]):
                    for n in args.vertices:
                        for p in args.edge_scaling_factor:
                            stat = dict()
                            stat["name"] = type(algorithm).__name__
                            stat["graph"] = selected_graph.__name__
                            stat["vertices"] = 0
                            stat["edge_scaling_factor"] = 1
                            stat["searcher"] = 1
                            stat["avg_degree"] = 0
                            stat["min_degree"] = 0
                            stat["samples"] = 0
                            stat["length"] = 0
                            stat["avg_len"] = 0
                            stat["pebbles"] = 0
                            stat["avg_pebbles"] = 0
                            # for potential use for confidence intervals
                            stat["tour_lengths"] = list()
                            stat["tour_pebbles"] = list()
                            stat["iteration"] = 0
                            stat["time"] = 0
                            stats[get_stats_id(type(algorithm).__name__, i, n, p, count)] = stat

        if selected_graph is not None:
            if not args.silent:
                print("Running " + type(algorithm).__name__ + " on " + selected_graph.__name__)
            # for progress update
            total_work = 0
            for alg in selected_algorithms:
                for n in args.vertices:
                    for p in args.edge_scaling_factor:
                        x = alg.work_required(n,p)
                        if alg.is_multi_iteration_supported():
                            x *= args.iterations
                        if alg.is_multi_searcher_supported():
                            x *= len(args.searcher)
                        x *= args.samples
                        total_work += x
            work_done = 0
            if args.progress:
                log_progress(work_done, total_work, total_start_time)
            random.seed(args.seed)
            for n in args.vertices:
                if not args.silent:
                    print("  n=" + str(n))
                for p in args.edge_scaling_factor:
                    if not args.silent:
                        print("  p=" + str(p))
                    for min_degree in args.min_degree:
                        seed = args.seed
                        for s in range(args.samples):
                            # create a new graph of the selected class
                            graph_orig = selected_graph(n=n, p= 1 / n * p, seed=seed, silent=args.silent, min_degree=min_degree)
                            ids = list(range(n))
                            if args.shuffle_ids:
                                random.shuffle(ids)
                            seed += 1
                            # let the algorithms run on the graph
                            for algorithm in selected_algorithms:
                                # only use a copy to ensure they dont interfere with one another
                                graph = copy.deepcopy(graph_orig)
                                for i in range(
                                    args.iterations
                                    if algorithm.is_multi_iteration_supported()
                                    else 1
                                ):
                                    for count in (args.searcher if algorithm.is_multi_searcher_supported() else [1]):
                                        if not args.silent:
                                            print("  Using "+str(count)+" seachers")
                                        start_time = time.time()
                                        traversal = algorithm.run(graph.G, 0, ids=ids, count=count)     
                                        work_done += algorithm.work_required(n, p)                       
                                        if args.progress:
                                            log_progress(work_done, total_work, total_start_time)
                                        timedelta = time.time() - start_time
                                        if args.show:
                                            if not args.silent:
                                                print("..draw")
                                            graph.draw(ids=ids, tour=traversal, layout=layout, prog=args.graphviz_prog, scale=args.scale)
                                        if args.show_latex:
                                            latex = graph.draw_latex(ids=ids, tour=traversal, args=sys.argv, layout=layout, prog=args.graphviz_prog, scale=args.scale)
                                            if args.show_latex_file:
                                                with open(args.show_latex_file, 'w') as f:
                                                    f.write(latex)
                                            else:
                                                if not args.silent:
                                                    print("---------[Latex]----------")
                                                print(latex)
                                                if not args.silent:
                                                    print("--------------------------")
                                        _register_stats(
                                            stats[get_stats_id(type(algorithm).__name__, i, n, p, count)],
                                            i,
                                            graph.G,
                                            traversal,
                                            timedelta,
                                            p,
                                            count
                                        )
            stats["info"]["time"] = time.time() - total_start_time
            if args.dump_raw:
                raw_json = json.dumps(stats, indent=4)
                if args.dump_raw_file:
                    with open(args.dump_raw_file, 'w') as f:
                        f.write(raw_json)
                else:
                    print(raw_json)
            
            # create a latex visualization
            for selected_visualizer in selected_visualizers:
                
                visualizer = selected_visualizer(args=sys.argv)
                latex = visualizer.visualize(stats=stats, algorithms=[type(alg).__name__ for alg in selected_algorithms], iterations=args.iterations, vertex_counts=args.vertices, edge_scaling_factors=args.edge_scaling_factor)
                
                if args.visualizer_files:
                    with open(args.visualizer_files[selected_visualizers.index(selected_visualizer)], 'w') as f:
                        f.write(latex)
                else:
                    print(latex)

    elif args.subcommand == "show":
        if not args.input:
            print("Please provide an input json file.")
            exit()
            
        if not os.path.exists(args.input):
            print("The file '"+args.input+"' does not exist.")
        
        if os.path.isdir(args.input):
            stats = dict()
            for root, dirs, files in os.walk(args.input, topdown=False):
                for file in files:
                    with open(file, "r") as f:
                        if file.endsWith(".json"):
                            stat = json.load(f)
                            for k in stat:
                                stats[k] = stat[k]
        elif os.path.isfile(args.input):
            with open(args.input, "r") as f:
                stats = json.load(f)
        else:
            print("Links are not supported.")
        
        visualizers = get_subclasses(TVisualizer)
        selected_visualizers = []
        for visualizer_name in args.visualizer:
            for visualizer in visualizers:
                if visualizer.__name__ == visualizer_name:
                    selected_visualizers.append(visualizer)
        if len(args.visualizer) > 0 and len(selected_visualizers) == 0:
            print("Could not find matching visualizer for '"+",".join(args.visualizer)+"'")
            print("Known Visualizers: "+",".join([visualizer.__name__ for visualizer in visualizers]))
            exit()

        if len(args.visualizer_files) != 0 and len(selected_visualizers) != len(args.visualizer_files):
            print("When specifying --visualizer-files as outputs, you need to match the number of outputs to the number of visualizers.")
            exit()
        
        for selected_visualizer in selected_visualizers:
            visualizer = selected_visualizer(args=sys.argv)
            latex = visualizer.vis(
                    stats=stats)
            if args.visualizer_files:
                with open(args.visualizer_files[selected_visualizers.index(selected_visualizer)], 'w') as f:
                    f.write(latex)
            else:
                print(latex)


if __name__ == "__main__":
    main()
