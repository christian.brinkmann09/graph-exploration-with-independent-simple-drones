from graph.tgraph import TDiGraph, TUnGraph

import networkx as nx


class CycleUnGraph(TUnGraph):

    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.Graph()
        for i in range(n):
            self.G.add_node(i)

        for i in self.G.nodes:
            self.add_edge(i, (i + 1) % len(self.G.nodes))


class CycleDiGraph(TDiGraph):

    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.DiGraph()
        for i in range(n):
            self.G.add_node(i)
            
        for i in self.G.nodes:
            self.add_edge(i, (i+1)%len(self.G.nodes))
