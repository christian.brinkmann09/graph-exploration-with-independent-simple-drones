from graph.tgraph import TDiGraph, TUnGraph

import math
    
scale = 1
    
class LadderUnGraph(TUnGraph):
    
    def __init__(self, n=10, p=0, seed=0, silent=False, min_degree=1):
        super().__init__()
        for i in range(n//2):
            self.G.add_node(2 * i)
            self.G.add_node(2 * i + 1)
            if i < n//2-1:
                self.add_edge(2 * i, 2 * i + 2)
                self.add_edge(2 * i + 3, 2 * i + 1)
            
            if i > 0:
                self.add_edge(2 * i, 2 * i + 1)
            else:
                self.add_edge(2 * i + 1, 2 * i)
        
        for i in range(n):
            self.set_pos({2 * i: (scale*i, 0), 2 * i + 1: (scale*i, -scale)})
            
class ExampleLadderUnGraph(TUnGraph):
    def __init__(self, n=10, p=0, seed=0, silent=False, min_degree=1):
        super().__init__()
        for i in range(n):
            self.G.add_node(i)
        for i in range(int(math.ceil(n/4))):
            self.add_edge_if_vertices_exist(i*4, i*4 + 1)
            self.add_edge_if_vertices_exist(i*4 + 1, i*4 + 2)
            self.add_edge_if_vertices_exist(i*4 + 2, i*4 + 3)
            self.add_edge_if_vertices_exist(i*4 + 3, i*4)
            self.add_edge_if_vertices_exist(4 * i + 1, 4 * (i+1))
            self.add_edge_if_vertices_exist(4 * i + 2, 4 * (i+1) + 3)
        
        for i in range(int(math.ceil(n/4))):
            d = {4*i: (2*scale*i, 0),
                          4*i+1: (2*scale*(i+0.5), 0),
                          4*i+3: (2*scale*i, -scale),
                          4*i+2: (2*scale*(i+0.5), -scale)}
            new_d = dict()
            for index in d:
                if index < n:
                    new_d[index] = d[index]
            self.set_pos(new_d)

class ExampleLadderDiGraph(TDiGraph):
    
    def __init__(self, n=10, p=0, seed=0, silent=False, min_degree=1):
        super().__init__()
        for i in range(n//2):
            self.G.add_node(2 * i)
            self.G.add_node(2 * i + 1)
            if i < n//2-1:
                self.add_edge(2 * i, 2 * i + 2)
                self.add_edge(2 * i + 3, 2 * i + 1)
            
            if i > 0:
                self.add_edge(2 * i, 2 * i + 1)
            else:
                self.add_edge(2 * i + 1, 2 * i)
        
        for i in range(n):
            self.set_pos({2 * i: (i, 0), 2 * i + 1: (i, -1)})