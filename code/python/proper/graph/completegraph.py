from graph.tgraph import TDiGraph, TUnGraph

import networkx as nx


class CompleteUnGraph(TUnGraph):
        
    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.Graph()
        for i in range(n):
            self.G.add_node(i)
        # ensures that the graph is connected
        for s in self.G.nodes:
            for t in self.G.nodes:
                if s != t:
                    self.add_edge(s, t)

class CompleteDiGraph(TDiGraph):
        
    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.DiGraph()
        # ensures that the graph is connected
        for s in self.G.nodes:
            for t in self.G.nodes:
                if s != t:
                    self.add_edge(s, t)
                    self.add_edge(t, s)