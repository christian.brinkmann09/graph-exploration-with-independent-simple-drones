import networkx as nx
import matplotlib.pyplot as plt
import random

from networkx.drawing.nx_pydot import graphviz_layout

# this class provides the basic functionality for graphs we use

def is_list_of_lists(parameter):
    # Check if parameter is a list
    if not isinstance(parameter, list):
        return False
    
    # Check if the list is empty
    if len(parameter) == 0:
        return False
    
    # Check if every element in the list is also a list
    return all(isinstance(sublist, list) for sublist in parameter)

# networkx doesnt render those symbols correctly...
def tally_mark_unicode(value):
    if not isinstance(value, int) or value < 0:
        raise ValueError("Tally marks only work for non-negative integers.")
    return '𝍸' * (value // 5) + '𝍷' * (value % 5)

def tally_mark_latex(value):
    if not isinstance(value, int) or value < 0:
        raise ValueError("Tally marks only work for non-negative integers.")
    text = '\\StrokeFive' * (value // 5)
    d = {1: "One", 2: "Two", 3: "Three", 4: "Four"}
    if (value % 5) in d:
        text += "\\Stroke"+d[(value % 5)]
    return text

def int_to_subscript(number):
    subscript_digits = str.maketrans("0123456789", "₀₁₂₃₄₅₆₇₈₉")
    return str(number).translate(subscript_digits)

class TGraph:

    G = None
    position_set = False
    
    def _get_rand_vertex_list(self, seed=0):
        l = list(self.G.nodes)
        random.seed(seed)
        random.shuffle(l)
        return l

    def set_pos(self, node_positions):
        self.position_set = True
        nx.set_node_attributes(self.G, node_positions, 'pos')

    def add_edge(self, u, v):
        self.G.add_edge(u, v)
        
    def add_edge_if_vertices_exist(self, u, v):
        if u in self.G.nodes and v in self.G.nodes:
            self.G.add_edge(u, v)
        
    def degree(self):
        return sum([x for x in nx.degree(self.G).values()])/len(self.G.nodes)

    def _draw(self, ids=None, tour=None, layout=nx.circular_layout, prog=None, scale=1.0):
        label_scale = 1
        if self.position_set:
            pos = nx.get_node_attributes(self.G, 'pos')
        else:
            if layout == graphviz_layout:
                label_scale = 100
            if prog is not None:
                pos = layout(self.G, prog=prog)
            else:
                pos = layout(self.G)
        edge_labels = nx.get_edge_attributes(self.G, 'label')
        
        pos = {k: (pos[k][0]*scale, pos[k][1]*scale) for k in pos}

        if ids:
            nx.set_node_attributes(self.G, {v: str(ids[v]) for v in ids}, 'label')

        if tour:
            if is_list_of_lists(tour):
                for t in tour:
                    label = dict()
                    for i in range(len(t) - 1):
                        edge = (t[i], t[i + 1])
                        if edge in label:
                            label[edge] += f",{i+1}"
                        elif (edge[1], edge[0]) in label:
                            label[(edge[1], edge[0])] += f",{i+1}"
                        else:
                            label[edge] = f"{i+1}"
                    for edge in label:
                        index = tour.index(t)
                        if edge in edge_labels:
                            edge_labels[edge] += "\n d"+int_to_subscript(index)+": "+label[edge]
                        else:
                            edge_labels[edge] = "d"+int_to_subscript(index)+": "+label[edge]
                    print(edge_labels)
            else:
                for i in range(len(tour) - 1):
                    edge = (tour[i], tour[i + 1])
                    if edge in edge_labels:
                        edge_labels[edge] += f",{i+1}"
                    elif (edge[1], edge[0]) in edge_labels:
                        edge_labels[(edge[1], edge[0])] += f",{i+1}"
                    else:
                        edge_labels[edge] = f"{i+1}"

            # Draw edge labels
            nx.draw_networkx_edge_labels(self.G, pos, edge_labels=edge_labels, font_color='red')

        if ids:
            nx.draw(self.G, pos, with_labels=True, font_weight='bold', node_size=700,
                node_color='black', font_color='white', labels=nx.get_node_attributes(self.G, 'label'))
        else:
            nx.draw(self.G, pos, with_labels=True, font_weight='bold', node_size=700,
                node_color='black', font_color='white')
            
        labels = dict()
        
        
        iteration = 0
        stop = False
        while not stop:
            stop = True
            for node in self.G.nodes:
                l = labels[node] if node in labels else ""
                visited_key = "visited_" + str(iteration)
                if visited_key in self.G.nodes[node] and self.G.nodes[node][visited_key]:
                    stop = False
                    l = l + ("\n" if len(l) > 0 else "") + "v"+int_to_subscript(iteration)+": "+str(self.G.nodes[node][visited_key])
                labels[node] = l
            iteration += 1
        #labels = {1: 'Node 1', 2: 'Node 2', 3: 'Node 3', 4: 'Node 4'}


        #if self.position_set:
        #label_pos = {k: (v[0], v[1] + 0.2) for k, v in pos.items()}
        if self.position_set is None and layout == nx.circular_layout:
            label_pos = {k: (v[0]* 1.3, v[1] * 1.3) for k, v in pos.items()} 
        else:
            label_pos = {k: (v[0] + 0.05 * label_scale * scale, v[1] + 0.2 * label_scale * scale) for k, v in pos.items()}
        
        #nx.draw(G, with_labels=True, labels=labels, font_weight='bold')
        
        nx.draw_networkx_labels(self.G, pos=label_pos, labels=labels, font_color='red', font_size=8, font_weight='bold')
            
        nx.draw_networkx_edge_labels(self.G, pos, edge_labels=edge_labels)
        plt.margins(0.2, 0.2)  # Set the margins manually if needed
        plt.gca().set_aspect('equal', adjustable='box')  # Maintain aspect ratio
        return plt

    def draw(self, ids=None, tour=None, layout=nx.circular_layout, prog=None, scale=1.0):
        plt = self._draw(ids=ids, tour=tour, layout=layout, prog=prog, scale=scale)
        plt.show()
        
    def _add_tikz_node(self, node, pos, label, scale=3):
        return "\\node[circle, draw, minimum size=10pt] ("+str(node)+") at ("+str(pos[node][0]*scale)+","+str(pos[node][1]*scale)+") {"+str(label if label is not None else node)+"};"
    
    def _add_tikz_edge(self, a, b, pos, label, directed=False, scale=1.0):
        if label is not None and label != "":
            edge_length = ((pos[b][0] - pos[a][0])**2 + (pos[b][1] - pos[a][1])**2)**0.5
            distance = max(0, 0.5 - (len(str(label)) * 10 / 12 / (edge_length * 30.0 * scale)))
            return """\\draw[-] ("""+str(a)+""") -- ($("""+str(a)+""") !"""+str(distance)+"""!("""+str(b)+""")$);
                    \\draw[-"""+(">" if directed else "")+"""] ($("""+str(a)+""")!"""+str(1-distance)+"""!("""+str(b)+""")$) -- ("""+str(b)+""");
                    
                    \\draw[draw=none] ("""+str(a)+""") -- node[midway, sloped] {"""+str(label)+"""} ("""+str(b)+""");
        """
        else:
            return "\\draw[-"+(">" if directed else "")+"] ("+str(a)+") to node[midway, above, sloped] {} ("+str(b)+");"
    
    def _add_tikz_label(self, id, label, pos=None, scale=3):
        if pos is None:
            return "\\node[anchor=west] at ("+str(id)+".east) {"+str(label)+"};"
        else:
            return "\\node[] at ("+str(scale*pos[0])+","+str(scale*pos[1])+") {"+str(label)+"};"

        
    def draw_latex(self, ids=None, tour=None, layout=nx.circular_layout, args=list(), prog=None, scale=1.0):
        latex = """
        \\begin{figure}[h]
            \\centering
                \\begin{tikzpicture}[>=latex,every text node part/.style={align=left}]
                    """
        label_scale = 1
        if self.position_set:
            pos = nx.get_node_attributes(self.G, 'pos')
        else:
            if self.position_set:
                pos = nx.get_node_attributes(self.G, 'pos')
            else:
                if layout == graphviz_layout:
                    label_scale = 100
            if prog:
                pos = layout(self.G, prog=prog)
                # Otherwise the graphviz coords are way too big
                scale = 0.1 * scale
            else:
                pos = layout(self.G)
        
        pos = {k: (pos[k][0]*scale, pos[k][1]*scale) for k in pos}
        
        for node in nx.nodes(self.G):
            latex += self._add_tikz_node(node, pos, ids[node] if ids is not None else None, scale=3*scale)
            latex += """
                    """
            
        edge_labels = nx.get_edge_attributes(self.G, 'label')
        
        if tour:
            for i in range(len(tour) - 1):
                edge = (tour[i], tour[i + 1])
                if edge in edge_labels:
                    edge_labels[edge] += f",{i+1}"
                elif (edge[1], edge[0]) in edge_labels:
                    edge_labels[(edge[1], edge[0])] += f",{i+1}"
                else:
                    edge_labels[edge] = f"{i+1}"
        
        for edge in nx.edges(self.G):
            label = None
            if (edge[0], edge[1]) in edge_labels:
                label = edge_labels[(edge[0], edge[1])]
            if (edge[1], edge[0]) in edge_labels:
                label = edge_labels[(edge[1], edge[0])]
            latex += self._add_tikz_edge(edge[0], edge[1], pos=pos, label=label, directed=nx.is_directed(self.G), scale=scale)
            latex += """
                    """
            
        labels = dict()
        iteration = 0
        stop = False
        use_tally_marks = True
        while not stop and use_tally_marks:
            stop = True
            visited_key = "visited_" + str(iteration)
            for node in self.G.nodes:
                if visited_key in self.G.nodes[node] and self.G.nodes[node][visited_key]:
                    stop = False
                    if self.G.nodes[node][visited_key] > 25:
                        use_tally_marks = False
                        break
            iteration += 1
        stop = False
        iteration = 0
        while not stop:
            stop = True
            for node in self.G.nodes:
                l = labels[node] if node in labels else ""
                visited_key = "visited_" + str(iteration)
                if visited_key in self.G.nodes[node] and self.G.nodes[node][visited_key]:
                    stop = False
                    l = l + "\\\\" + "$v_"+str(iteration)+"$: "+(tally_mark_latex(self.G.nodes[node][visited_key]) if use_tally_marks else str(self.G.nodes[node][visited_key]))
                labels[node] = l
            iteration += 1
        
        for node in labels:
            new_pos = (tuple([z * 1.3 for z in pos[node]]) if (self.position_set is None and layout == nx.circular_layout) else (pos[node][0]+0.05*2*label_scale*scale, pos[node][1]+0.2*2*label_scale*scale))
            latex += self._add_tikz_label(node,labels[node],pos=new_pos, scale=3*scale)
            latex += """
            """
            
        latex += """
                \end{tikzpicture}
            \caption{Example\\protect\\footnotemark}
            \label{fig:}
        \end{figure}
        
        \\footnotetext{
            Generated using \\texttt{"""+(" ".join(args)).replace("_","\\_")+"""}
        }
"""
        return latex

    def save(self, file, ids=None, tour=None):
        plt = self._draw(ids=ids, tour=tour)
        plt.gca().set_aspect('equal', adjustable='box')  # Maintain aspect ratio
        plt.savefig(file)

    def clear_visited(self):
        iteration = 0
        iterationExists = True
        while iterationExists:
            iterationExists = False
            for node in self.G.nodes:
                visited_key = "visited_" + str(iteration)
                if visited_key in self.G.nodes[node]:
                    iteration += 1
                    iterationExists = True
                    break
        for it in range(iteration):
            visited_key = "visited_" + str(it)
            for node in self.G.nodes:
                if visited_key in self.G.nodes[node]:
                    del self.G.nodes[node][visited_key]
    
# Visualize the graph with edge labels

class TDiGraph(TGraph):

    def __init__(self, G=None):
        self.G = nx.DiGraph() if G is None else G

class TUnGraph(TGraph):

    def __init__(self, G=None):
        self.G = nx.Graph() if G is None else G