from graph.tgraph import TUnGraph

import networkx as nx


class FullTreeUnGraph(TUnGraph):

    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.full_rary_tree(min_degree, n)
