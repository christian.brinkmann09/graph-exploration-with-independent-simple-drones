
from visualization.tvisualizer import TVisualizer, latex_colors, get_stats_id

import sys

class ComparedToOffline(TVisualizer):

    def visualize(self, stats, algorithms, iterations, vertex_counts, edge_scaling_factors):
        result = """
        \\begin{figure}[htbp]
            \\centering
            \\begin{tikzpicture}[scale=1]
                \\begin{axis}[
                    xlabel={$n$: vertices in graph},
                    ylabel={tour length / optimal tour length},
                    axis line style={->},
                    width=0.8\\textwidth,
                    height=0.5\\textwidth,
                    grid=both,
                    grid style={line width=.1pt, draw=gray!10},
                    major grid style={line width=.2pt,draw=gray!50},
                    legend pos=outer north east,
                    ]"""
        
        color_index = 0
        for edge_scaling_factor in edge_scaling_factors:
            offline_solution = dict()
            for vc in vertex_counts:
                stats_id = get_stats_id("OfflineSolver", 0, vc, edge_scaling_factor)
                offline_solution[vc] = stats[stats_id]["avg_len"]
                
            for algorithm in algorithms:
                if algorithm != "OfflineSolver" and algorithm != "MultiGreedyVertexCount":
                    header = """
                    \\addplot[""" + latex_colors[color_index%len(latex_colors)] + ", thick] coordinates {"""
                    content = ""
                    color_index += 1
                    for iteration in range(1):
                        for vc in vertex_counts:
                            stats_id = get_stats_id(algorithm, iteration, vc, edge_scaling_factor)
                            if stats_id in stats and offline_solution[vc] != 0:
                                stat = stats[stats_id]
                                title = stat["name"].replace("GreedyVertexCount", "GVC") + "($\\alpha$=" + "{:.2f}".format(edge_scaling_factor)+")"

                                content += "(" + str(stat["vertices"]) + "," + str(stat["avg_len"] / offline_solution[vc]) + ")"
                            else:
                                title = None
                        if title is not None:
                            footer = (
                        """};
                        \\addlegendentry{"""+ title+ """
                        }""")
                            result += header + content + footer + "\n"
        result += """
                    \\end{axis}
                \\end{tikzpicture}
                \\caption{"""+(", ".join(algorithms))+""" running on """+str(stat["samples"])+""" samples of """+stat["graph"]+""", edge probability $p=\\frac{\\alpha}{n}$ \\protect\\footnotemark}

            \\label{fig:}
        \\end{figure}
        
        \\footnotetext{
            Visualized using \\texttt{"""+(" ".join(sys.argv)).replace("_","\\_")+"""}. Data generated using \\texttt{"""+str(self.args).replace("_","\\_")+""" in """+str(int(stats["info"]["time"])) +""" seconds.}
        }
        """
        return result