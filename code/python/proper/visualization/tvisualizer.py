
latex_colors = [
    "red",
    "green",
    "blue",
    "yellow",
    "magenta",
    "cyan",
    "gray",
    "olive",
    "brown",
    "orange",
    "black",
]

def get_stats_id(algorithm, iteration, n, p, searcher=1):
    return str(algorithm)+ "_" + str(iteration) + "_" + str(n) + "_" + str(p) + "_" + str(searcher)

class TVisualizer:
    
    def vis(self, stats):
        # find algorithms
        # find iterations
        # find vertex counts
        # find edge_scaling_factors
        # find seachers
        algoritms = set()
        iterations = 1
        vertices = set()
        edge_scaling_factors = set()
        seachers = set()
        self.args = ""
        for key in stats:
            if key != "info":
                algoritms.add(stats[key]["name"])
                iterations = max(iterations, stats[key]["iteration"])
                vertices.add(stats[key]["vertices"])
                edge_scaling_factors.add(stats[key]["edge_scaling_factor"])
                seachers.add(stats[key]["searcher"])
        self.args = stats["info"]["args"]
            
        return self.visualize(stats, list(algoritms), iterations, list(vertices), list(edge_scaling_factors))
        
    
    args = None
    
    def __init__(self, args=list):
        self.args = args
        

    def visualize(self, stats, algorithms, iteration, vertex_counts, edge_scaling_factors):
        raise NotImplementedError