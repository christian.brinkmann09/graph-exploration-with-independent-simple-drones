from graph.tgraph import TDiGraph, TUnGraph

import random
import networkx as nx

class RandUnGraph(TUnGraph):
        
    def __init__(self, n=10, p=0.3, seed=0, silent=False, min_degree=1):
        super().__init__()
        
        self.G = nx.gnp_random_graph(n, p, directed=False, seed=seed)
        if not nx.is_connected(self.G):
            changed = True
            while changed:
                changed = False
                for s in self._get_rand_vertex_list(seed=seed+1):
                    for t in self._get_rand_vertex_list(seed=seed+2):
                        if not nx.has_path(self.G, s, t):
                            if s != t:
                                self.add_edge(s, t)
                                changed = True
                                break
                    if changed:
                        break
                seed+=1
        if min_degree < len(self.G.nodes):
            for s in self._get_rand_vertex_list(seed=seed):
                if self.G.degree(s) < min_degree:
                    for t in self._get_rand_vertex_list(seed=seed):
                        if s != t and not self.G.has_edge(s, t):
                            if self.G.degree(s) < min_degree:
                                self.add_edge(s, t)
            
class RandDiGraph(TDiGraph):    
    def __init__(self, n=10, p=0.3, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.fast_gnp_random_graph(n, p, directed=True, seed=seed)
        if not nx.is_strongly_connected(self.G):
            changed = True
            while changed:
                changed = False
                for s in self._get_rand_vertex_list(seed=seed):
                    for t in self._get_rand_vertex_list(seed=seed):
                        if not nx.has_path(self.G, s, t):
                            if s != t:
                                self.add_edge(s, t)
                                changed = True
                                break
                    if changed:
                        break
                seed+=1
        if min_degree < len(self.G.nodes):
            for s in self._get_rand_vertex_list(seed=seed):
                if self.G.degree(s) < min_degree:
                    for t in self._get_rand_vertex_list(seed=seed):
                        if s != t and not self.G.has_edge(s, t):
                            if self.G.degree(s) < min_degree:
                                self.add_edge(s, t)

class RandTreeUnGraph(TUnGraph):

    def __init__(self, n=10, p=1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.random_tree(n=n, seed=seed)
        
        
class RandHamUnGraph(TUnGraph):

    def __init__(self, n=10, p=0.1, seed=0, silent=False, min_degree=1):
        super().__init__()
        self.G = nx.Graph()
        for i in range(n):
            self.G.add_node(i)

        for i in self.G.nodes:
            self.add_edge(i, (i + 1) % len(self.G.nodes))

        rands = [random.random() for _ in range(n*n)]
        for s in range(n):
            for t in range(n):
                if rands[s*n+t] < p:
                    if not self.G.has_edge(s, t) and s != t:
                        self.add_edge(s, t)
        
        if min_degree < len(self.G.nodes):
            for s in self._get_rand_vertex_list(seed=seed):
                seed += 1
                if self.G.degree(s) < min_degree:
                    for t in self._get_rand_vertex_list(seed=seed):
                        seed += 1
                        if s != t and not self.G.has_edge(s, t):
                            if self.G.degree(s) < min_degree:
                                self.add_edge(s, t)